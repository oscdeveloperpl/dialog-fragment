package pl.oscdeveloper.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;

public class ProgressFragment extends DialogFragment {

  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    LayoutInflater inflater = getActivity().getLayoutInflater();
    builder.setView(inflater.inflate(R.layout.progress, null));
    builder.setTitle(R.string.wyjscie);
    return builder.create();
  }

  // zapobiega zniszczeniu fragmentu
  @Override
  public void onStart() {
    super.onStart();
    setRetainInstance(true);
  }

  // zapobiega zniknieciu fragmentu dialogu przy zmianie orientacji
  @Override
  public void onDestroyView() {
    if (getDialog() != null && getRetainInstance()) {
      getDialog().setOnDismissListener(null);
    }
    super.onDestroyView();
  }
}