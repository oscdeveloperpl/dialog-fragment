package pl.oscdeveloper.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;


public class ShutdownFragment extends DialogFragment {
  private ShutdownListener listener;

  public interface ShutdownListener {
    void onYesClick();
    void onNoClick();
  }

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    try {
      listener = (ShutdownListener) activity;
    } catch (ClassCastException e) {
      throw new ClassCastException(activity.getClass().getName()
          + " zaimplementuj " + ShutdownListener.class.getName());
    }
  }

  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    builder.setTitle(R.string.wyjscie);
    builder.setMessage(R.string.pytanie);
    builder.setPositiveButton(R.string.tak, new OnClickListener() {

      @Override
      public void onClick(DialogInterface dialog, int which) {
        listener.onYesClick();
      }
    });
    builder.setNegativeButton(R.string.nie, new OnClickListener() {

      @Override
      public void onClick(DialogInterface dialog, int which) {
        listener.onNoClick();
      }
    });
    return builder.create();
  }

}