package pl.oscdeveloper.dialog;

import pl.oscdeveloper.dialog.ShutdownFragment.ShutdownListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements ShutdownListener {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    if (savedInstanceState == null) {
      getSupportFragmentManager().beginTransaction()
          .add(R.id.container, new PlaceholderFragment()).commit();
    }
  }


  public static class PlaceholderFragment extends Fragment {
    private Button exitButton;

    public PlaceholderFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
      super.onActivityCreated(savedInstanceState);
      exitButton = (Button) getActivity().findViewById(R.id.exitButton);
      exitButton.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View v) {
          FragmentTransaction ft = getActivity()
              .getSupportFragmentManager().beginTransaction();

          DialogFragment dialog = new ShutdownFragment();
          dialog.show(ft, "sDialog");
        }
      });
    }
  }

  @Override
  public void onYesClick() {
    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
    final DialogFragment dialog = new ProgressFragment();
    dialog.show(ft, "pDialog");
    new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          Thread.sleep(4000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        dialog.dismiss();
        finish();
      }
    }).start();
  }

  @Override
  public void onNoClick() {
  }

}